extends Node2D

enum ShootingModes {NONE, PUNCH, GUN, PISS, BOOM}
var mode = ShootingModes.NONE


var punchEffect = 'res://PunchEffect.tscn'
var gunEffect = 'res://GunEffect.tscn'
var gunWound = 'res://GunshotWound.tscn'

var zShake = 0
var sShake = 0

var hp = 500
var zvaariTargetable = false

var pwned = false

var hurtsfx = [
	"res://middlea/zvaari/mario-cough.WAV",
	"res://middlea/zvaari/mario-die.WAV",
	"res://middlea/zvaari/mario-doh.WAV",
	"res://middlea/zvaari/mario-fire.WAV",
	"res://middlea/zvaari/mario-lowonhealth.WAV",
	"res://middlea/zvaari/mario-mamamia.WAV",
	"res://middlea/zvaari/mario-oof.WAV",
	"res://middlea/zvaari/mario-pain.WAV",
	"res://middlea/zvaari/mario-scream.WAV",
	"res://middlea/zvaari/mario-ugh.WAV",
	"res://middlea/zvaari/mario-ungh.WAV"
]
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if zShake > 1:
		zShake -= delta * 100
	else:
		zShake = 0
	
	if pwned && $PwnTimer.is_stopped():
		$PwnTimer.start()
	
	
	
	match mode:
		ShootingModes.PUNCH:
			if Input.is_action_just_pressed("fire") && zvaariTargetable:
				var nE = load(punchEffect).instance()
				nE.global_position = get_viewport().get_mouse_position()
				add_child(nE)
				$PunchPlayer.play()
				hp -= 1
				hurtZvaari()
		ShootingModes.GUN:
			if Input.is_action_just_pressed("fire") && zvaariTargetable:
				var nE = load(gunEffect).instance()
				nE.global_position = get_viewport().get_mouse_position()
				add_child(nE)
				$GunPlayer.play()
				hp -= 5
				hurtZvaari()
		ShootingModes.PISS:
			if Input.is_action_pressed("fire") && zvaariTargetable:
				$piss1.visible = true
				$PissPlayer.volume_db = 0
				sShake = 40
				hp -= delta * 20
				hurtZvaari()
				
		ShootingModes.BOOM:
			if Input.is_action_just_pressed("fire") && zvaariTargetable:
				$EndPlayer.play("end")
				$Punch.visible = false
				$Gun.visible = false
				$Piss.visible = false
				$Boom.visible = false
				$FunnyPlayer.stop()
				mode = ShootingModes.NONE
				pass
	
	if Input.is_action_just_released("fire"):
			$piss1.visible = false
			$PissPlayer.volume_db = -80
			sShake = 0

func hurtZvaari():
	$ZVAARI.frame = 0
	zShake = 40
	
	if hp > 470:
		$ZVAARI.play("hurt0")
	elif hp > 350:
		$ZVAARI.play("hurt1")
		if $Gun.visible == false:
			$Gun.visible = true
			$BlipPlayer.play()
	elif hp > 200:
		$ZVAARI.play("hurt2")
		if $Piss.visible == false:
			$Piss.visible = true
			$BlipPlayer.play()
	else:
		$ZVAARI.play("hurt3")
		if $Boom.visible == false:
			$Boom.visible = true
			$BlipPlayer.play()

	hurtsfx.shuffle()
	var sfx = load(hurtsfx[0])
	$HurtPlayer.stream = sfx
	$HurtPlayer.play()

func _on_Punch_pressed():
	$BleepPlayer.play()
	$Gun.pressed = false
	$Piss.pressed = false
	$Boom.pressed = false
	mode = ShootingModes.PUNCH


func _on_Gun_pressed():
	$BleepPlayer.play()
	$Punch.pressed = false
	$Piss.pressed = false
	$Boom.pressed = false
	mode = ShootingModes.GUN


func _on_Piss_pressed():
	$BleepPlayer.play()
	$Punch.pressed = false
	$Gun.pressed = false
	$Boom.pressed = false
	mode = ShootingModes.PISS

func _on_Boom_pressed():
	$BleepPlayer.play()
	$Punch.pressed = false
	$Gun.pressed = false
	$Piss.pressed = false
	mode = ShootingModes.BOOM



func _on_ZeHurtTimer_timeout():
	randomize()
	$ZVAARI.position = Vector2(900 + rand_range(zShake, -zShake) , 780 + rand_range(zShake, -zShake))
	$Camera2D.global_position = Vector2(0 + rand_range(sShake, -sShake) , 0 + rand_range(sShake, -sShake))


func _on_Area2D_mouse_entered():
	zvaariTargetable = true
	pass # Replace with function body.


func _on_Area2D_mouse_exited():
	zvaariTargetable = false
	pass # Replace with function body.

func activatePwn():
	pwned = true


func _on_PwnTimer_timeout():
	var np = load('res://ZVAARIPWNED.tscn').instance()
	add_child(np)
	$PwnTimer.start(rand_range(0.3, 1.5))


func _on_TextureButton_pressed():
	$PerfectBG.visible = false
	$PerfectPlayer.stop()
	$FunnyPlayer.play()
	pass # Replace with function body.
