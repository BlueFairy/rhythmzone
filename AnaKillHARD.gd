extends Node2D


var time_begin
var time_delay

var beatnumber = 0

var beatmap = [8.360, 9.344, 10.327, 10.573, 11.311, 12.295, 13.278, 14.754, 15.000, 
15.245, 16.229, 17.213, 18.196, 19.180, 19.426, 20.163, 21.147, 22.622,
22.868, 23.114, 24.098, 25.081, 26.065, 27.540, 28.524, 29.508,

31.996, 32.173, 32.351, 33.416, 33.593, 33.771, 34.481, 35.191, 35.901, 36.433, 36.611,
37.321, 37.854, 38.741, 39.274, 40.161, 40.694, 41.404, 41.581, 42.114, 42.291,
43.001, 43.534, 44.067, 44.599, 45.132, 46.197, 46.374, 46.552, 47.262, 47.972,
48.682, 48.859, 49.214, 49.392, 49.925, 50.635, 50.812, 51.522, 51.700, 52.055, 52.232, 52.942, 53.120, 53.297, 53.830,

54.362, 54.540, 54.895, 55.072, 55.427, 55.605, 55.960, 56.138, 56.493, 57.025, 57.203, 57.558, 57.735, 58.445, 58.623, 58.800, 59.333,
60.043, 60.576, 61.108, 61.641, 61.819, 62.351, 62.529, 62.884, 63.416, 63.949, 64.836, 65.014, 65.191,
65.724, 66.256, 66.789, 67.499, 68.032, 68.209, 68.742, 69.274, 70.162, 70.339, 70.517,
71.049, 71.582, 72.114, 72.292, 72.647, 73.179, 73.357, 73.712, 74.245, 74.955, 75.132, 76.020, 76.197, 76.375,

76.907, 77.085, 77.440, 77.795, 77.972, 78.505, 79.037, 79.750,  80.458, 80.635, 80.813, 80.990, 81.523, 82.055,

99999
]
var beattypemap = [0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0,

31, 32, 33, 31, 32, 33, 4, 4, 4, 21, 22,
1, 1, 1, 1, 1, 1, 21, 22, 21, 22,
1, 1, 1, 1, 1, 31, 32, 33, 4, 4,
21, 22, 21, 22, 4, 21, 22, 21, 22, 21, 22, 31, 32 ,33, 4,

21, 22, 21, 22, 21, 22, 21, 22, 4, 21, 22, 21, 22, 31, 32, 33, 4,
1, 1, 1, 21, 22, 21, 22, 1, 1, 1, 31, 32, 33,
1, 1, 1, 4, 21, 22, 1, 1, 31, 32, 33,
1, 1, 21, 22, 4, 21, 22, 1, 1, 21, 22, 31, 32, 33,

21, 22, 21, 22, 4, 1, 1, 1, 31, 32, 33, 1, 1, 1,

0
]

var cuenumber = 0

var cuemap = [31.285, 32.706, 34.126, 34.836, 35.546, 36.078, 36.256,
36.996, 37.498, 38.386, 38.919, 39.806, 40.339, 41.049, 41.226, 41.759, 41.936,
42.646, 43.179, 43.712, 44.244, 44.777, 45.487, 46.907, 47.617,
48.327, 48.504, 48.859, 49.037, 49.569, 50.280, 50.457, 51.167, 51.345, 51.700, 51.877, 52.232, 53.475,

54.007, 54.185, 54.540, 54.717, 55.072, 55.250, 55.605, 55.783, 56.138, 56.670, 56.848, 57.203, 57.380, 57.735, 58.978, 
59.688, 60.221, 60.753, 61.286, 61.463, 61.996, 62.174, 62.529, 63.061, 63.594, 64.126,
65.369, 65.901, 66.434, 67.144, 67.677, 67.854, 68.387, 68.919, 69.452, 70.694,
71.227, 71.759, 71.937, 72.292, 72.824, 73.002, 73.534, 74.067, 74.600, 74.777, 75.310, 76.552, 76.730,

77.085, 77.262, 77.617, 78.150, 78.682, 79.215, 79.748, 80.990, 81.523, 82.055,

99999, 99999
]
var cuetypemap = [3, 3, 4, 4, 4, 21, 22,
1, 1, 1, 1, 1, 1, 21, 22, 21, 22,
1, 1, 1, 1, 1, 3, 4, 4,
21, 22, 21, 22, 4, 21, 22, 21, 22, 21, 22, 3, 4,

21, 22, 21, 22, 21, 22, 21, 22, 4, 21, 22, 3, 4,
1, 1, 1, 21, 22, 21, 22, 1, 1, 1, 3, 
1, 1, 1, 4, 21, 22, 1, 1, 3, 1,
1, 21, 22, 4, 21, 22, 1, 1, 21, 22, 3, 21, 22,

21, 22, 4, 1 ,1 ,1, 3, 1, 1, 1,

0, 0
]

var graceTime = 0.06

var beatTime = 0.4918
var beatTimeInc = 0.4918


var lastBeatInput = false

var time

var beatAnim = "beat"
var neutralAnim = "neutral"

func _ready():
	time_begin = OS.get_ticks_usec()
	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()



func _process(delta):
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	
	# Obtain from ticks.
	time = (OS.get_ticks_usec() - time_begin) / 1000000.0
	# Compensate for latency.
	time -= time_delay
	# May be below 0 (did not begin yet).
	time = max(0, time)
#	print("Time is: ", time)
	if time >= 31.285 && beatAnim == "beat":
		beatAnim = "beat1"
		neutralAnim = "neutral1"

	
	if time >= beatTime:
		if $turdl3/AnimationPlayer.current_animation == "beat" || !$turdl3/AnimationPlayer.is_playing():
			$turdl3/AnimationPlayer.stop()
			$turdl3/AnimationPlayer.play(beatAnim)
		if $deo/AnimationPlayer.current_animation == "beat" || !$deo/AnimationPlayer.is_playing():
			$deo/AnimationPlayer.stop()
			$deo/AnimationPlayer.play("beat")
		
		if beatTime >= 29.508:
			beatTimeInc = 0.539
		if beatTime >= 30.047:
			beatTimeInc = 0.460
		if beatTime >= 30.507:
			beatTimeInc = 0.408
		if beatTime >= 30.915:
			beatTimeInc = 0.370
		if beatTime >= 31.285:
			$deo.visible = true
			beatTimeInc = 0.35502

		beatTime += beatTimeInc
	
#	autoplay
#	if time >= beatmap[beatnumber]:
#		input1()
	
	if Input.is_action_just_pressed("hit1"):
		input1()
	
	if time >= cuemap[cuenumber]:
		match cuetypemap[cuenumber]:
			4:
				$deo/AnimationPlayer.play("cue4")
			3, 31, 32, 33:
				$deo/AnimationPlayer.play("cue3")
			21:
				$deo/AnimationPlayer.play("cue21")
			22:
				$deo/AnimationPlayer.play("cue22")
			_:
				$deo/AnimationPlayer.play("cue1")
		cuenumber += 1
	
	
	if time >= beatmap[beatnumber] + graceTime:
		beatnumber += 1
		miss()


	

func input1():
	print("diff = ",time - beatmap[beatnumber])
	if time >= beatmap[beatnumber] - graceTime && time <= beatmap[beatnumber] + graceTime:
		hit()
		beatnumber += 1
	else:
		miss()
#	print("time: ", time, ", next beat: ", beatmap[beatnumber])

func hit():
	print("beat number is ", beatnumber)
	clear()
	$turdl3/AnimationPlayer.stop()
	$deo/AnimationPlayer.stop()
	match beattypemap[beatnumber]:
		4:
			$turdl3/AnimationPlayer.play("hit4")
		31:
			$turdl3/AnimationPlayer.play("hit31")
			$deo/AnimationPlayer.play("hit3")
		32:
			$turdl3/AnimationPlayer.play("hit32")
			$deo/AnimationPlayer.play("hit3")
		33:
			$turdl3/AnimationPlayer.play("hit33")
			$deo/AnimationPlayer.play("hit3")
		21:
			$turdl3/AnimationPlayer.play("hit21")
			$deo/AnimationPlayer.play("hit2")
		22:
			$turdl3/AnimationPlayer.play("hit22")
			$deo/AnimationPlayer.play("hit2")
		0:
			$turdl3/AnimationPlayer.play("hit")
		_:
			$turdl3/AnimationPlayer.play("hit1")
			$deo/AnimationPlayer.play("hit1")



	$HitPlayer.play()

func miss():
	if $MissTimer.is_stopped():
		$MissTimer.start()
		$MissPlayer.play()
		
#		var deocue = $deo/AnimationPlayer.current_animation
		var deocue
		if cuenumber > 0:
			deocue = cuetypemap[cuenumber - 1]
		else:
			deocue = 0
		clear()
		$turdl3/AnimationPlayer.stop()
		$deo/AnimationPlayer.stop()
		
		print(deocue)
		match deocue:
			4:
				$turdl3/AnimationPlayer.play("miss4")
			3:
				$turdl3/AnimationPlayer.play("miss3")
				$deo/AnimationPlayer.play("hit3")
			21, 22:
				$turdl3/AnimationPlayer.play("miss2")
				$deo/AnimationPlayer.play("hit2")
			0:
				$turdl3/AnimationPlayer.play("miss")
			_:
				$turdl3/AnimationPlayer.play("miss1")
				$deo/AnimationPlayer.play("hit1")

func _on_Timer_timeout():
	$AudioStreamPlayer2D.play()
	pass # Replace with function body.


func _on_Timer2_timeout():
	$AudioStreamPlayer2D2.play()
	pass # Replace with function body.


func _on_AnimationPlayer_animation_finished(anim_name):
	
	if !$turdl3/AnimationPlayer.is_playing() && anim_name != neutralAnim:
#		print(anim_name, " finished playing, changin to neutral")
		$turdl3/AnimationPlayer.play(neutralAnim)
	pass # Replace with function body.

func clear():
	$deo/holyfire1.texture = null
	$deo/holyfire2.texture = null
	$deo/holyfire3.texture = null
	$turdl3/CHAIN.texture = null
	$turdl3/bullet.texture = null
