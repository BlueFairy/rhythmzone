extends TextureRect

var textFinished = false

# Called when the node enters the scene tree for the first time.
func _ready():
#	_changeText("GOD I LOVE ASTOLFO SO MUCH")
	pass # Replace with function body.
func _changeText(text):
	textFinished = false
	$Label.text = text
	$Label.visible_characters = 1
	$TextTimer.start()
	pass
	



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextTimer_timeout():
	if $Label.visible_characters < $Label.text.length():
		$Label.visible_characters += 1
		$BlipPlayer.play()
		$TextTimer.start()
	else:
		textFinished = true
		$BlipPlayer.stop()

func finishShowingText():
	$Label.visible_characters = $Label.text.length()
	$BleepPlayer.play()
	textFinished = true
