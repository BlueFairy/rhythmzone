extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity = Vector2(0,0)
var rotSpeed

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	velocity.x = rand_range(-200, -1200)
	velocity.y = rand_range(-800, -2400)
	rotSpeed = rand_range(-20, -360)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity.y += 1
	position = position + (velocity * delta)
	rotation_degrees = rotation_degrees + (rotSpeed * delta)
	if position.y > 2000:
		queue_free()
