extends Node2D


var time_begin
var time_delay

var beatnumber = 0
var nextBeat = 0.0

var beatmap = [1.071, 3.214, 5.357, 6.964, 8.035, 9.642, 11.785, 12.857, 13.928, 16.607, 9999999.9]
var graceTime = 0.05

func _ready():
	time_begin = OS.get_ticks_usec()
	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()
	nextBeat = beatmap[beatnumber]
	print(time_delay)


func _process(delta):
	# Obtain from ticks.
	var time = (OS.get_ticks_usec() - time_begin) / 1000000.0
	# Compensate for latency.
	time -= time_delay
	# May be below 0 (did not begin yet).
	time = max(0, time)
#	print("Time is: ", time)
	
	if Input.is_action_just_pressed("hit1"):
		if time > beatmap[beatnumber] - graceTime && time < beatmap[beatnumber] + graceTime:
			hit()
		else:
			miss()
		print("diff = ",time - beatmap[beatnumber])
	
	if time > beatmap[beatnumber] + graceTime:
		beatnumber += 1
	


func hit():
	$HitPlayer.play()
	$Good/AnimationPlayer.play("beat")	

func miss():
	$MissPlayer.play()
	$Bad/AnimationPlayer.play("beat")

func _on_Timer_timeout():
	$AudioStreamPlayer2D.play()
	pass # Replace with function body.


func _on_Timer2_timeout():
	$AudioStreamPlayer2D2.play()
	pass # Replace with function body.
