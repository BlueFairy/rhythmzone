extends Node2D

enum GameStates { TUTINTRO1, TUTORIAL1, TUTINTRO2, TUTORIAL2, TUTINTRO3, TUTORIAL3, TUTINTRO4, 
TUTORIAL4, MAIN, MAININTRO, INTRO1, INTRO2, END}
var gameState = GameStates.TUTINTRO1

var time_begin
var time_delay

var beatnumber = 0

var Tbeatmap = [
	[0.545, 1.636, 2.727, 4.090],
	[0.483, 0.725, 1.693, 1.935, 2.661, 2.903],
	[0.674, 1.682, 2.696, 4.044],
	[0.923, 1.153, 1.384, 3.000, 3.230, 3.461]
	]
var tutorialHitCounter = 0
var tutorialStageCounter = 0
var tutorialLoopTime = 0
var tutorialTextI = 0

var beatmap = [8.360, 9.344, 10.327, 10.573, 11.311, 12.295, 13.278, 14.754, 15.000, 
15.245, 16.229, 17.213, 18.196, 19.180, 19.426, 20.163, 21.147, 22.622,
22.868, 23.114, 24.098, 25.081, 26.065, 27.540, 28.524,

31.996, 32.173, 32.351, 33.416, 33.593, 33.771, 34.481, 35.191, 35.901, 36.433, 36.611,
37.321, 37.854, 38.741, 39.274, 40.161, 40.694, 41.404, 41.581, 42.114, 42.291,
43.001, 43.534, 44.067, 44.599, 45.132, 46.197, 46.374, 46.552, 47.262, 47.972,
48.682, 48.860, 49.570, 49.748, 50.280, 50.458, 50.990, 51.168, 51.523, 52.055, 52.943, 53.120, 53.298,

54.185, 54.363, 54.895, 55.073, 55.606, 55.783, 56.671, 57.381, 57.558, 58.446, 58.623, 58.801, 59.333,
60.043, 60.576, 61.463, 61.996, 62.706, 62.884, 63.416, 64.304, 64.836, 
65.724, 66.256, 67.144, 67.677, 68.564, 69.097, 69.984, 70.162, 70.339, 
71.582, 71.759, 72.114, 72.292, 72.824, 73.534, 74.422, 74.600, 74.955, 75.132, 75.665, 76.375,

77.262, 77.440, 77.795, 77.972, 78.505, 79.037, 79.925, 80.103, 80.635, 81.345, 81.878,
82.786, 82.943, 83.120, 83.475, 84.185, 84.718, 85.606, 85.783, 86.316, 87.026, 87.558, 88.091, 88.623, 89.156,

99999
]
var beattypemap = [0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0, 0, 0, 
0, 0, 0, 0, 0, 0, 0,

31, 32, 33, 31, 32, 33, 4, 4, 4, 21, 22,
1, 1, 1, 1, 1, 1, 21, 22, 21, 22,
1, 1, 1, 1, 1, 31, 32, 33, 4, 4,
21, 22, 21, 22, 21, 22, 21, 22, 1, 1, 31, 32, 33,

21, 22, 21, 22, 21, 22, 4, 21, 22, 31, 32, 33, 4,
1, 1, 1, 1, 21, 22, 1, 1, 4, 
1, 1, 1, 1, 1, 1, 31, 32, 33,
21, 22, 21, 22, 1, 1, 21, 22, 21, 22, 4, 4,

21, 22, 21, 22, 1, 1, 21, 22, 1, 1, 1,
31, 32, 33, 1, 1, 1, 21, 22, 1, 1, 1, 4, 4, 4,

0
]

var cuenumber = 0

var cuemap = [31.285, 32.706, 34.126, 34.836, 35.546, 36.078, 36.256,
36.996, 37.498, 38.386, 38.919, 39.806, 40.339, 41.049, 41.226, 41.759, 41.936,
42.646, 43.179, 43.712, 44.244, 44.777, 45.487, 46.907, 47.617,
48.327, 48.505, 49.215, 49.392, 49.925, 50.103, 50.635, 50.813, 51.168,  51.700, 52.233, 

53.830, 54.008, 54.540, 54.718, 55.250, 55.428, 56.316, 57.026, 57.203, 57.736, 58.978,
59.511, 60.221, 61.108, 61.641, 62.351, 62.529, 63.061, 63.949, 64.481,
65.369, 65.901, 66.789, 67.321, 68.209, 68.742, 69.274,
71.227, 71.404, 71.759, 71.937, 72.469, 73.179, 74.067, 74.245, 74.600, 74.777, 75.310, 76.020,

76.907, 77.085, 77.440, 77.617, 78.150, 78.682, 79.570, 79.748, 80.280, 80.990, 81.523, 82.055,
83.120, 83.830, 84.363, 85.250, 85.428, 85.961, 86.671, 87.203, 87.736, 88.268, 88.801,

99999, 99999
]
var cuetypemap = [3, 3, 4, 4, 4, 21, 22,
1, 1, 1, 1, 1, 1, 21, 22, 21, 22,
1, 1, 1, 1, 1, 3, 4, 4,
21, 22, 21, 22, 21, 22, 21, 22, 1, 1, 3,

21, 22, 21, 22, 21, 22, 4, 21, 22, 3, 4,
1, 1, 1, 1, 21, 22, 1, 1, 4,
1, 1, 1, 1, 1, 1, 3,
21, 22, 21, 22, 1, 1, 21, 22, 21, 22, 4, 4,

21, 22, 21, 22, 1, 1, 21, 22, 1, 1, 1, 3,
1, 1, 1, 21, 22, 1, 1, 1, 4, 4, 4,


0, 0
]

var graceTime = 0.06

var beatTime = 0.4918
var beatTimeInc = 0.4918


var lastBeatInput = false

var time

var beatAnim = "beat"
var neutralAnim = "neutral"

var cutsceneFlags = [false, false, false]

onready var tutAudio1 = load('res://music/tutorialAversion.ogg')
onready var tutCue1 = load('res://music/tutorialCue1.ogg')
onready var tutAudio2 = load('res://music/tutorialfuckversion.ogg')
onready var tutCue2 = load('res://music/tutorialCue2.ogg')
onready var tutAudio3 = load('res://music/tutorialbitchversion.ogg')
onready var tutCue3 = load('res://music/tutorialCue3.ogg')
onready var tutAudio4 = load('res://music/tutorialKKKKKKKversion.ogg')
onready var tutCue4 = load('res://music/tutorialCue4.ogg')
onready var mainAudio = load('res://music/ultrataptrial.ogg')
onready var mainCue = load('res://music/cuetrackE.ogg')
onready var endLSong = load('res://music/epicfail.ogg')
onready var endOSong = load('res://music/epicok.ogg')
onready var endWSong = load('res://music/epicwin.ogg')
onready var endLBite = load('res://music/failEnd.ogg')
onready var endOBite = load('res://music/okEnd.ogg')
onready var endWBite = load('res://music/winEnd.ogg')

var text = [
	[
		"it's time for some ultra tap trial",
		"let's practice a bit first (not that i need it)",
		"i just need to press A after the monkey goes a"
	],
	[
		"ez",
		"time for those double ones",
		"these are the ones that usually get you"
	],
	[
		"yeah",
		"time for the jump ones",
		"not much difference, just press A lol"
	],
	[
		"nice",
		"now for the triple ones",
		"a bit trickier but not too hard"
	],
		[
		"thats enough tutorial",
		"let's get started before someone wakes up",
		"they would probably land me in jail for this"
	],
	[
		"when lights go out...",
		"the creatures\nof the night",
		"come out to play..."
	],
	[
		"some videogames of course!",
		"with everyone asleep",
		"i can finally play some rhythm heaven 284910284901284"
	],
]
var texti = 0

var score = 0
var endLineNumber = 4
var endResult = 0

var cringe1 = 'res://cringe1.tscn'
var cringe2 = 'res://cringe2.tscn'

var perfectRun = true
var autoplay = false

func _ready():
	$StartAnimationPlayer.play("start")
	pass

func _start(what):
	$AudioStreamPlayer2D.stop()
	$CuePlayer.stop()
	time = 0
	time_begin = OS.get_ticks_usec()
	time_delay = AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency()
	beatnumber = 0
	tutorialLoopTime = 0
	
	
	match what:
		GameStates.MAIN:
			$AudioStreamPlayer2D.stream = mainAudio
			$CuePlayer.stream = mainCue
			beatTime = 0.4918
			beatTimeInc = 0.4918
			$P.visible = true
			$PerfectText/PTextPlayer.play("blink")
			$TutorialText.visible = false
		GameStates.TUTORIAL1:
			$AudioStreamPlayer2D.stream = tutAudio1
			$CuePlayer.stream = tutCue1
			beatTime = 0.545
			beatTimeInc = 0.545
			$TutorialText.visible = true
		GameStates.TUTORIAL2:
			$AudioStreamPlayer2D.stream = tutAudio2
			$CuePlayer.stream = tutCue2
			beatTime = 0.483
			beatTimeInc = 0.483
			$TutorialText.visible = true			
		GameStates.TUTORIAL3:
			$AudioStreamPlayer2D.stream = tutAudio3
			$CuePlayer.stream = tutCue3
			beatTime = 0.674
			beatTimeInc = 0.674
			$TutorialText.visible = true			
		GameStates.TUTORIAL4:
			$AudioStreamPlayer2D.stream = tutAudio4
			$CuePlayer.stream = tutCue4
			beatTime = 0.461
			beatTimeInc = 0.461
			$TutorialText.visible = true
		GameStates.END:
			$ResultsBlack/AnimationPlayer.play("end")
			$ResultsBlack/EndTimer.start(3)
			if score <= 0:
				$ResultsBlack/Thoughts.text = "cringe."
				endLineNumber = 1
				endResult = 0
			elif score < 137:
				$ResultsBlack/Thoughts.text = "Wow you are so good at catching bullets!\nToo bad you were supposed to dodge them."
				endLineNumber = 2
				endResult = 0
			elif score < 242:
				$ResultsBlack/Thoughts.text = "Not bad actually, you didn't get completely murdered.\nYou're at least better at this than you are at being a detective.\nWhich is not saying much..."
				endLineNumber = 3
				endResult = 1
			elif score < 274:
				$ResultsBlack/Thoughts.text = "Damn.\nThat was.\nEpic..."
				endLineNumber = 3
				endResult = 2
			else:
				$ResultsBlack/Thoughts.text = "HOW"
				endLineNumber = 1
				endResult = 2
			
	
	if what != GameStates.END:
		$AudioStreamPlayer2D.play()
		$CuePlayer.play()
	
	gameState = what

func _startTutIntro(what):
	$AudioStreamPlayer2D.stop()
	$CuePlayer.stop()
	$TutorialText.visible = false
	
	match what:
		GameStates.TUTINTRO1:
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[0][0])
			tutorialTextI = 0
			texti = 1
		GameStates.TUTINTRO2:
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[1][0])
			tutorialTextI = 1
			texti = 1
		GameStates.TUTINTRO3:
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[2][0])
			tutorialTextI = 2
			texti = 1
		GameStates.TUTINTRO4:
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[3][0])
			tutorialTextI = 3
			texti = 1
		GameStates.MAININTRO:
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[4][0])
			tutorialTextI = 4
			texti = 1
		GameStates.INTRO1:
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[5][0])
			tutorialTextI = 5
			texti = 1
		GameStates.INTRO2:
			$MiscPlayer.play()
			$TEXTBUBBLE.visible = true
			$TEXTBUBBLE._changeText(text[6][0])
			$turdl3.set("modulate", Color(1,1,1,1))
			$Dark.visible = false
			tutorialTextI = 6
			texti = 1
		
	gameState = what


func _process(delta):
	if Input.is_action_just_pressed("reset"):
		 get_tree().change_scene("res://Anatolian Killer.tscn")
	
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	
	if Input.is_action_just_pressed("skip") && gameState != GameStates.MAIN:
		$turdl3.set("modulate", Color(1,1,1,1))
		$Dark.visible = false
		$TEXTBUBBLE.visible = false
		$StartAnimationPlayer.stop()
		_start(GameStates.MAIN)
	
	
	match gameState:
		GameStates.TUTINTRO1, GameStates.TUTINTRO2, GameStates.TUTINTRO3, GameStates.TUTINTRO4, GameStates.MAININTRO, GameStates.INTRO1, GameStates.INTRO2:
			if Input.is_action_just_pressed("hit1"):
				if texti < 3:
					if $TEXTBUBBLE.textFinished:
						$TEXTBUBBLE._changeText(text[tutorialTextI][texti])
						texti += 1
					else:
						$TEXTBUBBLE.finishShowingText()
				else:
					if !$TEXTBUBBLE.textFinished:
						$TEXTBUBBLE.finishShowingText()
					else:
						$TEXTBUBBLE.visible = false
						match gameState:
							GameStates.TUTINTRO1:
								_start(GameStates.TUTORIAL1)
							GameStates.TUTINTRO2:
								_start(GameStates.TUTORIAL2)
							GameStates.TUTINTRO3:
								_start(GameStates.TUTORIAL3)
							GameStates.TUTINTRO4:
								_start(GameStates.TUTORIAL4)
							GameStates.MAININTRO:
								_start(GameStates.MAIN)
							GameStates.INTRO1:
								_startTutIntro(GameStates.INTRO2)
							GameStates.INTRO2:
								_startTutIntro(GameStates.TUTINTRO1)
						

		GameStates.MAIN, GameStates.TUTORIAL1, GameStates.TUTORIAL2, GameStates.TUTORIAL3, GameStates.TUTORIAL4:
			# Obtain from ticks.
			time = (OS.get_ticks_usec() - time_begin) / 1000000.0
			# Compensate for latency.
			time -= time_delay
			# May be below 0 (did not begin yet).
			time = max(0, time)
		#	print("Time is: ", time)
			continue

		GameStates.TUTORIAL1, GameStates.TUTORIAL2, GameStates.TUTORIAL3, GameStates.TUTORIAL4:
			if time >= beatTime:
				if $turdl3/AnimationPlayer.current_animation == "beat" || !$turdl3/AnimationPlayer.is_playing():
					$turdl3/AnimationPlayer.stop()
					$turdl3/AnimationPlayer.play(beatAnim)
				$Stars/AnimationPlayer.play("beat")
				beatTime += beatTimeInc
			
			$TutorialText.text = "%d more to go!" % (3 - tutorialStageCounter)
			continue

		GameStates.TUTORIAL1:
			if Input.is_action_just_pressed("hit1"):
				inputT(0)
			
			if tutorialHitCounter >= 4:
				tutorialHitCounter = 0
				tutorialStageCounter += 1
			
			if beatnumber > 3:
				beatnumber = 0
				tutorialLoopTime += 4.363

			if time >= Tbeatmap[0][beatnumber] + graceTime + tutorialLoopTime:
				beatnumber += 1
				missT()
			
			if beatnumber > 3:
				beatnumber = 0
				tutorialLoopTime += 4.363
			
			if tutorialStageCounter >= 3:
				tutorialStageCounter = 0
				_startTutIntro(GameStates.TUTINTRO2)
		
		GameStates.TUTORIAL2:
			if Input.is_action_just_pressed("hit1"):
				inputT(1)
			
			if tutorialHitCounter >= 6:
				tutorialHitCounter = 0
				tutorialStageCounter += 1
			
			if beatnumber > 5:
				beatnumber = 0
				tutorialLoopTime += 3.870

			if time >= Tbeatmap[1][beatnumber] + graceTime + tutorialLoopTime:
				beatnumber += 1
				missT()
			
			if beatnumber > 5:
				beatnumber = 0
				tutorialLoopTime += 3.870
			
			if tutorialStageCounter >= 3:
				tutorialStageCounter = 0
				_startTutIntro(GameStates.TUTINTRO3)
		
		GameStates.TUTORIAL3:
			if Input.is_action_just_pressed("hit1"):
				inputT(2)
			
			if tutorialHitCounter >= 4:
				tutorialHitCounter = 0
				tutorialStageCounter += 1
			
			if beatnumber > 3:
				beatnumber = 0
				tutorialLoopTime += 5.393

			if time >= Tbeatmap[2][beatnumber] + graceTime + tutorialLoopTime:
				beatnumber += 1
				missT()
			
			if beatnumber > 3:
				beatnumber = 0
				tutorialLoopTime += 5.393
			
			if tutorialStageCounter >= 3:
				tutorialStageCounter = 0
				_startTutIntro(GameStates.TUTINTRO4)

		
		GameStates.TUTORIAL4:
			if Input.is_action_just_pressed("hit1"):
				inputT(3)
			
			if tutorialHitCounter >= 6:
				tutorialHitCounter = 0
				tutorialStageCounter += 1
			
			if beatnumber > 5:
				beatnumber = 0
				tutorialLoopTime += 3.692

			if time >= Tbeatmap[3][beatnumber] + graceTime + tutorialLoopTime:
				beatnumber += 1
				missT()
			
			if beatnumber > 5:
				beatnumber = 0
				tutorialLoopTime += 3.692
			
			if tutorialStageCounter >= 3:
				tutorialStageCounter = 0
				_startTutIntro(GameStates.MAININTRO)


		GameStates.MAIN:
			if time >= 29.508 && beatAnim == "beat":
				beatAnim = "beat1"
				neutralAnim = "neutral1"
				$turdl3/AnimationPlayer.stop()
				$turdl3/AnimationPlayer.play("inter2")

			if time >= 25.574 && cutsceneFlags[0] == false:
				cutsceneFlags[0] = true
				$TEXTBUBBLE/AnimationPlayer.stop()
				$TEXTBUBBLE/AnimationPlayer.play("intermission")

			if time >= 30.915 && cutsceneFlags[1] == false:
				cutsceneFlags[1] = true
				$turdl3/AnimationPlayer.stop()
				$turdl3/AnimationPlayer.play("inter3")
			
			if time >= beatTime:
				if $turdl3/AnimationPlayer.current_animation == "beat" || !$turdl3/AnimationPlayer.is_playing():
					$turdl3/AnimationPlayer.stop()
					$turdl3/AnimationPlayer.play(beatAnim)
				if $deo/AnimationPlayer.current_animation == "beat" || !$deo/AnimationPlayer.is_playing():
					$deo/AnimationPlayer.stop()
					$deo/AnimationPlayer.play("beat")
				$Stars/AnimationPlayer.play("beat")
				
				if beatTime >= 29.508:
					beatTimeInc = 0.539
				if beatTime >= 30.047:
					beatTimeInc = 0.460
				if beatTime >= 30.507:
					beatTimeInc = 0.408
				if beatTime >= 30.915:
					beatTimeInc = 0.370
				if beatTime >= 31.285:
					beatTimeInc = 0.35502
				
				if beatTime >= 92.931:
					_start(GameStates.END)

				beatTime += beatTimeInc
			
			if Input.is_action_just_pressed("autoplay"):
				autoplay = !autoplay
				$AutoText.visible = autoplay
#			autoplay
			if autoplay:
				if time >= beatmap[beatnumber] - AudioServer.get_time_to_next_mix():
					input1()

			if Input.is_action_just_pressed("hit1"):
				input1()
			
			if time >= cuemap[cuenumber]:
				match cuetypemap[cuenumber]:
					4:
						$deo/AnimationPlayer.play("cue4")
					3, 31, 32, 33:
						$deo/AnimationPlayer.play("cue3")
					21:
						$deo/AnimationPlayer.play("cue21")
					22:
						$deo/AnimationPlayer.play("cue22")
					_:
						$deo/AnimationPlayer.play("cue1")
				cuenumber += 1
			
			
			if time >= beatmap[beatnumber] + graceTime:
				beatnumber += 1
				miss()
				
		GameStates.END:
			if Input.is_action_just_pressed("hit1") && $ResultsBlack/EndTimer.is_stopped():
				showEnding()
				$ResultsBlack/EndTimer.start(99999)

	

func input1():
	print("diff = ",time - beatmap[beatnumber])
	if time >= beatmap[beatnumber] - (graceTime + 0.2) && time <= beatmap[beatnumber] + graceTime:
		hit()
		beatnumber += 1
	else:
		miss()
#	print("time: ", time, ", next beat: ", beatmap[beatnumber])

func inputT(var bm):
	if time >= Tbeatmap[bm][beatnumber] + tutorialLoopTime - (graceTime + 0.2) && time <= Tbeatmap[bm][beatnumber] + tutorialLoopTime + graceTime:
		hitT()
		beatnumber += 1
		tutorialHitCounter += 1
	else:
		missT()
	


func hit():
	score += 2
	print("beat number is ", beatnumber)
	print("beat time is ", beatmap[beatnumber])
	clear()
	
	if perfectRun:
		$PerfectText/AnimationPlayer.stop()
		$PerfectText/AnimationPlayer.play("hit")
	
	if $turdl3/AnimationPlayer.current_animation != "inter2" && $turdl3/AnimationPlayer.current_animation != "inter3":
		$turdl3/AnimationPlayer.stop()
		$deo/AnimationPlayer.stop()
		match beattypemap[beatnumber]:
			4:
				$turdl3/AnimationPlayer.play("hit4")
			31:
				$turdl3/AnimationPlayer.play("hit31")
				$deo/AnimationPlayer.play("hit3")
			32:
				$turdl3/AnimationPlayer.play("hit32")
				$deo/AnimationPlayer.play("hit3")
			33:
				$turdl3/AnimationPlayer.play("hit33")
				$deo/AnimationPlayer.play("hit3")
			21:
				$turdl3/AnimationPlayer.play("hit21")
				$deo/AnimationPlayer.play("hit2")
			22:
				$turdl3/AnimationPlayer.play("hit22")
				$deo/AnimationPlayer.play("hit2")
			0:
				$turdl3/AnimationPlayer.play("hit")
			_:
				$turdl3/AnimationPlayer.play("hit1")
				$deo/AnimationPlayer.play("hit1")
	$HitPlayer.play()

func hitT():
	print("beat number is ", beatnumber)
	print("beat time is ", beatmap[beatnumber])
	clear()
	
	$turdl3/AnimationPlayer.stop()
	$deo/AnimationPlayer.stop()
	$turdl3/AnimationPlayer.play("hit")

	$HitPlayer.play()

func miss():
	if $MissTimer.is_stopped() && $turdl3/AnimationPlayer.current_animation != "inter2" && $turdl3/AnimationPlayer.current_animation != "inter3":
		score -= 1
		
		if perfectRun:
			$PerfectText/AnimationPlayer.play("miss")
			$PerfectText/PTextPlayer.play("miss")
			$PerfectFailPlayer.play()
			perfectRun = false
		
		$MissTimer.start()
		$MissPlayer.play()
		
		var c1 = load(cringe1).instance()
		var c2 = load(cringe2).instance()
		c1.global_position = Vector2(691, 729)
		c2.global_position = Vector2(691, 729)
		add_child(c1)
		add_child(c2)
		
#		var deocue = $deo/AnimationPlayer.current_animation
		var deocue
		if cuenumber > 0:
			deocue = cuetypemap[cuenumber - 1]
		else:
			deocue = 0
		clear()
		$turdl3/AnimationPlayer.stop()
		$deo/AnimationPlayer.stop()
		
		print(deocue)
		match deocue:
			4:
				$turdl3/AnimationPlayer.play("miss4")
			3:
				$turdl3/AnimationPlayer.play("miss3")
				$deo/AnimationPlayer.play("hit3")
			21, 22:
				$turdl3/AnimationPlayer.play("miss2")
				$deo/AnimationPlayer.play("hit2")
			0:
				$turdl3/AnimationPlayer.play("miss")
			_:
				$turdl3/AnimationPlayer.play("miss1")
				$deo/AnimationPlayer.play("hit1")

func missT():
	if $MissTimer.is_stopped():
		tutorialHitCounter = 0
		$MissTimer.start()
		$MissPlayer.play()
		
		var c1 = load(cringe1).instance()
		var c2 = load(cringe2).instance()
		c1.global_position = Vector2(691, 729)
		c2.global_position = Vector2(691, 729)
		add_child(c1)
		add_child(c2)
		
		$turdl3/AnimationPlayer.stop()
		$deo/AnimationPlayer.stop()
		$turdl3/AnimationPlayer.play("miss")

func _on_Timer_timeout():
	$AudioStreamPlayer2D.play()
	pass # Replace with function body.


func _on_Timer2_timeout():
	$AudioStreamPlayer2D2.play()
	pass # Replace with function body.


func _on_AnimationPlayer_animation_finished(anim_name):
	
	if !$turdl3/AnimationPlayer.is_playing() && anim_name != neutralAnim:
#		print(anim_name, " finished playing, changin to neutral")
		$turdl3/AnimationPlayer.play(neutralAnim)
	pass # Replace with function body.

func clear():
	$deo/holyfire1.texture = null
	$deo/holyfire2.texture = null
	$deo/holyfire3.texture = null
	$turdl3/CHAIN.texture = null
	$turdl3/bullet.texture = null


func _on_EndTimer_timeout():
	if !perfectRun:
		if $ResultsBlack/Title.visible != true:
			$ResultsBlack/Title.visible = true
			$ResultsBlack/EndTimer.start(1.5)
			$TEXTBUBBLE/BleepPlayer.play()
		elif $ResultsBlack/Thoughts.visible != true:
			$ResultsBlack/Thoughts.visible = true
			$ResultsBlack/Thoughts.max_lines_visible = 1
			$ResultsBlack/EndTimer.start(1.5)
			$TEXTBUBBLE/BlipPlayer.play()
		elif $ResultsBlack/Thoughts.max_lines_visible < endLineNumber - 1:
			$ResultsBlack/Thoughts.max_lines_visible = $ResultsBlack/Thoughts.max_lines_visible + 1
			$ResultsBlack/EndTimer.start(1.5)
			$TEXTBUBBLE/BlipPlayer.play()
		elif $ResultsBlack/Thoughts.max_lines_visible == endLineNumber - 1:
			$ResultsBlack/Thoughts.max_lines_visible = endLineNumber
			$ResultsBlack/EndTimer.start(2.2)
			$TEXTBUBBLE/BlipPlayer.play()
		else:
			match endResult:
				0:
					$ResultsBlack/RankL.visible = true
					$EndingPlayer.stream = endLSong
					$EndBitePlayer.stream = endLBite
				1:
					$ResultsBlack/RankO.visible = true
					$EndingPlayer.stream = endOSong
					$EndBitePlayer.stream = endOBite
				2:
					$ResultsBlack/RankW.visible = true
					$EndingPlayer.stream = endWSong
					$EndBitePlayer.stream = endWBite
			$EndingPlayer.play()
	else:
		get_tree().change_scene("res://Kill ZVAARI.tscn")

func showEnding():
	match endResult:
		0:
			$EndAnimationPlayer.play("endL")
		1:
			$EndAnimationPlayer.play("endO")
		2:
			$EndAnimationPlayer.play("endW")
